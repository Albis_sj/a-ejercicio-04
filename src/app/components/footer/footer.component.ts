import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  diaActual = new Date ();
  dias = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']
  diaN = this.diaActual.getDate()
  diaA = this.dias[this.diaN];
  


  mesActual = new Date ();
  meses: string[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
  month = this.meses [this.mesActual.getMonth()];
  anioActual = new Date();
  year = this.anioActual.getFullYear();

  constructor() { }

  ngOnInit(): void {
  }

}
